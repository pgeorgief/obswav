OBSWAV

A tool to take a list of Edgetech commands and generate the WAV files for those commands.  This allows one to use an IPOD or similar device to play the commands out in the lab (for testing purposes).

An Acoustics.csv file is included as an example.
