#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
###########################################################################
# Copyright (c) 2013 U.C. San Diego
#
# Filename:    OBSWAV.py
# Version:     0.1
# Date:        Sat Dec 29 07:11:32 2012
# Author:      Paul Georgief
# Email:       pgeorgief@gmail.com
# Purpose:     Take a list of EdgeTech/ORE codes and generate .wav files
#              so that the codes can be played with a digital music player
#              for in-air testing of EdgeTech releases.
# License:     This is released under the GPL or BSD license - your choice.
#              Use, copy, and distribute as you wish.
#
# Thanks also to Phil Thai and Chris Judge for allowing me to bother you
# guys during this long holiday cruise.  Also, special thanks to John Collins
# who doesn't know I'm working on this during his obs pickup cruise.
###########################################################################
"Paul's EdgeTech code generator."

import sys
import wave
import math
import struct
import StringIO
import os
import os.path
from optparse import OptionParser

# GLOBALS
PROGRAM_NAME = "OBSWAV"
VERSION = "0.1 (20121228)"
WAVE_DIR = "Waves"
DEFAULT_FILE = "Acoustics.csv"
CONTACT = "Paul Georgief (pgeorgief@gmail.com)"


def print_logo():
    print """
   ____  ____ ______       _____ _    __
  / __ \/ __ / ___| |     / /   | |  / /
 / / / / __  \__ \| | /| / / /| | | / /
/ /_/ / /_/ ___/ /| |/ |/ / ___ | |/ /
\____/_____/____/ |__/|__/_/  |_|___/
"""
    print PROGRAM_NAME
    print "Version: ", VERSION
    print "Contact: ", CONTACT
    print "UCSD OBS LAB"
    print


def print_info():
    print """
OBSWAV creates audio .wav files for use in acoustic bench testing of Edgetec/ORE
underwater acoustic communications system.  There are 4 ways to use this program:

         1) obswav
         2) obswav -f someFile.csv
         3) obswav -c 312543
         4) obswav -p 11000

Example 1 uses a default file to process a comma seperated valued (csv) file.  The
default filename is '%s'.

The first row in the file must be the column identifiers.  The first column must be
the identification number of the device.  This is important since the file names used
to name the files are are based on the first row and first column.  Use this method
to batch process any number of codes. Note: Missing codes are ignored.  An example
of a file is given below:

    SN,ENABLE,DISABLE,BURN1,BURN2,OPT
    001,310242,310261,326251,326272,
    002,307117,307134,325105,325126,302516
    <continues>

This will generate files named: SN001_ENABLE_310242.wav, SN001_DISABLE_310261.wav, ...
All generated files are stored in the 'Waves' directory.  If the directory does not exist
then it will be created in the current working directory.

Example 2 allows users to use a csv filename other than the default as an input file.

Example 3 creates a single file corresponding to the code given.  The output
file defaults to the code with a '.wav' extension.  The generated file is stored
in the current working directory - not in the 'Waves' directory.

Example 4 creates a single ping at the given frequency.  The output file
defaults to the prefix "PING_" followed by the ping frequency and ending
with a '.wav' extension.  The generated files are stored in the current working
directory - not in the 'Waves' directory.
""" % DEFAULT_FILE


def get_options():
    "Obtain user options from the command line."
    usageStr = "%prog [-f csvFile] [-c AcousticCode] [-p PingFrequency] [-i]"
    parser = OptionParser(usageStr)
    parser.add_option("-i", "--info", dest="info",
                      action="store_true", help="Display program information and examples", default=False)

    parser.add_option("-f", "--file", dest="csvFile",
                      action="store", type="string", help="Change the default input filename", default=DEFAULT_FILE)

    parser.add_option("-c", "--code", dest="acousticCode",
                      action="store", type="int", help="Create .wav for given code", default=None)

    parser.add_option("-p", "--ping", dest="pingFrequency",
                      action="store", type="int", help="Create .wav for a ping at given frequency", default=None)

    parser.add_option("-v", "--version", dest="version",
                      action="store_true", help="Display program version", default=False)

    (opt, arguments) = parser.parse_args()

    if opt.version:
        print "%s Version %s" % (PROGRAM_NAME, VERSION)
        print "Copyright (c) 2013 University of California, San Diego"
        print "License: GPL or BSD - your choice"
        print "This is free software: you are free to change and redistribute it."
        print "There is NO WARRANTY, to the extent permitted by law."
        print
        print "Written By: ", CONTACT
        sys.exit(0)

    if opt.info:
        print_logo()
        print_info()
        sys.exit(0)

    if len(arguments) != 0:
        print_logo()
        print_info()
        # parser.print_help()
        sys.exit(0)

    return (opt.csvFile, opt.acousticCode, opt.pingFrequency)


def create_sine_wave(frequency, sampleRate, duration):
    """
    Return a binary encoded string of 16-bit data values representing a sampled sine wave.
    The max/min data value for the .wav data is +/-32760 (this differs from 16-bit max/min).

    'frequency'    - Frequency of the sinusoidal wave
    'sampleRate'   - Sample rate of the digital system - standards sample rates
                     are 8KHz (Telephone), 44.1KHz (CD quality), 48KHz (DAT), etc.
                     The values are is Samples-Per-Second (e.g., for CD quality use 44100)
    'duration'     - Number of seconds the sine wave should last (e.g., use 0.05 for 50msec).
    """
    swave = StringIO.StringIO()
    nPoints = int(round(duration * sampleRate))
    for n in range(nPoints):
        value = math.sin(2*math.pi * frequency * n / sampleRate)
        value = int(value * 32760)  # MAKE IT AN INTEGER
        swave.write(struct.pack('<h', value))  # pack it into a string buffer (short, signed, litte endian)

    # Return a string containing binary data
    return swave.getvalue()


def write_wave_file(filename, audioData, sampleRate):
    """
    Write the data out as a wave file.

    'filename'     - The entire path to the file
    'audioData'    - The audio/signal to save to the file
    'sampleRate'   - The sample rate at which the audio data was recorded.
    """
    wfile = wave.open(filename, 'wb')

    # Set Parameters (interlace data for multi channels: Left, Right)
    wfile.setnchannels(1)           # 1 Channel (MONO -- not stereo)
    wfile.setsampwidth(2)           # 2-Bytes (16-bits "CD Quality") samples
    wfile.setframerate(sampleRate)  # Sample Rate (samples per second)
    wfile.setnframes(sampleRate*2)  # SampleRate * NumberOfChannels * BytesPerSample
    wfile.setcomptype('NONE', 'noncompressed')  # No compression

    # Write the file
    wfile.writeframes(audioData)
    wfile.close()


def get_FSK(code):
    """
    Returns the frequencies and bit pattern to use for sending an acoustic signal.

    'code'         - A 6-digit command code from the edgetech box (as an integer)

    Return Value is a tuple: (f0, f1, binary)
               f0  - Frequency used to send a '0' bit
               f1  - Frequency used to send a '1' bit
           binary  - A string of 16 binary digits of 0's and 1's

    Background
    The EdgeTech box uses frequency shift keying to transmit data to a bottom package.  The code is
    a 6-digit number.  The first number in the code indicates which two frequencies will be used
    for the transmission - this is the called the tone Pair.

    The other 5-digits are the code - in octal form producing a 15-bits of the code.  A parity bit
    is attached at the end.  The final result is a 16-bit value that is transmitted by the box to
    the bottom package.
    """
    scode = "%06d" % code
    tpair, code = scode[0], scode[1:]

    # I'm missing tone pair 7?????
    tonePairs = [[9488,  9901], [9488, 10288], [9488, 10684],
                 [9901, 10288], [9901, 10684], [10288, 10684]]

    f0, f1 = tonePairs[int(tpair) - 1]
    binary = [((int(code, 8) >> (14-i)) & 1) for i in range(15)]

    # Add in parity bit (even)
    binary.append(sum(binary) % 2)

    return (f0, f1, binary)


def create_code_wave(code, fs):
    """
    Returns 'wav' data corresponding to a tone.

    'code'         - A 6-digit command code from the edgetech box (as an integer)

    The commands are created from separate 'wav' data and stiched together.  Each
    tone is 20msec in duration with 230msec delay between tones (for a 250msec period).
    There is an additional 5sec delay between the 8th and 9th pulse (It looks like this
    was done to let the capacitors on the primary side of the transformer charge up
    before the next 8-bit round of transmissions).
    """
    (f0, f1, binary) = get_FSK(code)
    wfm = ""
    for n in range(16):
        f = [f0, f1][binary[n]]
        wfm = wfm + create_sine_wave(f, fs, 0.020)  # Tone (20 msec)
        wfm = wfm + create_sine_wave(0, fs, 0.230)  # Delay 230 msec
        if n == 7:
            wfm = wfm + create_sine_wave(0, fs, 5.000)  # Delay 5 sec
    wfm = wfm + create_sine_wave(0, fs, 10.000)  # Delay 10 sec
    return wfm


def create_ping_wave(f, fs):
    """
    Create PING waveform.

    'f'            - The ping frequency (e.g.: 11000, 13000, etc.)
    'fs'           - Sample rate (For CD quality: 44100)
    """
    wfm = create_sine_wave(f, fs, 0.023)  # Ping
    wfm = wfm + (create_sine_wave(0, fs, 1.000))  # delay 1 seconds
    return wfm


def create_multiple_pings(f, fs):
    """
    Create multiple PING waveforms.  This is a special request of 3 pings with
    a 5 second delay between pings.

    'f'            - The ping frequency (e.g.: 11000, 13000, etc.)
    'fs'           - Sample rate (For CD quality: 44100)
    """
    wfm = create_sine_wave(f, fs, 0.023)  # Ping
    wfm = wfm + (create_sine_wave(0, fs, 5.000))   # delay 5 seconds
    wfm = wfm + (create_sine_wave(f, fs, 0.023))   # Ping
    wfm = wfm + (create_sine_wave(0, fs, 5.000))   # delay 5 seconds
    wfm = wfm + (create_sine_wave(f, fs, 0.023))   # Ping
    wfm = wfm + (create_sine_wave(0, fs, 10.000))  # delay 10 seconds
    return wfm


# ----------------------------------------------------------------------------------
# Check for script invocation (run if the script is not imported as a module)
# ----------------------------------------------------------------------------------
if __name__ == '__main__':

    # ====================
    # Main Code Generation
    # ====================
    (optFile, optCode, optPing) = get_options()
    sampleRate = 44100

    if optPing is not None:
        fname = "PING_%s.wav" % str(optPing)
        # print "Generating Ping: %s" % fname
        wfm = create_ping_wave(optPing, sampleRate)
        write_wave_file(fname, wfm, sampleRate)
        sys.exit(0)

    if optCode is not None:
        fname = "%s.wav" % str(optCode)
        # print "Generating Code: %s" % fname
        wfm = create_code_wave(int(optCode), sampleRate)
        write_wave_file(fname, wfm, sampleRate)
        sys.exit(0)

    if optFile is not None:
        if not os.path.exists(optFile):
            print "Error: The file '%s' does not exists." % optFile
            print "For more help use the switches -i for info/examples and -h for simple help."
            sys.exit(1)
        aList = [j.split(',') for j in [i.strip() for i in (open(optFile, 'rU').readlines())]]
        codeType = aList[0][1:]

        if not os.path.exists(WAVE_DIR):
            os.makedirs(WAVE_DIR)

        # Write Pings (3 Ping frequencies since Chris doesn't remember what he needs)
        print "Generating PINGS..."
        for f, desc in ((11000, '11KHz'),
                        (12000, '12KHz'),
                        (13000, '13KHz')):
            fname = os.path.join(WAVE_DIR, "PING_%s.wav" % desc)
            print "    %s" % fname
            wfm = create_multiple_pings(f, sampleRate)
            write_wave_file(fname, wfm, sampleRate)
        print

        # Write Codes
        print "Generating Codes..."
        for acoustic in aList[1:]:
            sn, codes = acoustic[0], acoustic[1:]
            for i, code in enumerate(codes):
                if code != '':
                    fname = os.path.join(WAVE_DIR, "SN%s_%s_%s.wav" % (sn, codeType[i], code))
                    print "    %s" % fname
                    wfm = create_code_wave(int(code), sampleRate)
                    write_wave_file(fname, wfm, sampleRate)
